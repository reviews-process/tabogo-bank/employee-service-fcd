CREATE TABLE employees (
	id_number VARCHAR(30) NOT NULL,
	full_name VARCHAR(100) NOT NULL,
	area VARCHAR(6) NOT NULL,
	role VARCHAR(6) NOT NULL,
	boss_id VARCHAR(30),
	
	PRIMARY KEY(id_number),
	FOREIGN KEY (boss_id) REFERENCES employees(id_number)
);