package org.review.employee.entities;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;

@Data
@Entity
@Table(name = "employees")
public class Employee {
	
	@Id
	@Column
	private String idNumber;

	@Column
	@NotBlank(message = "Full Name field cannot be empty or null")
	private String fullName;
	
	@Column
	@NotBlank(message = "Area field cannot be empty or null")
	private String area;
	
	@Column
	@NotBlank(message = "Role Field cannot be empty or null")
	private String role; // function was changed to role.
	
	@ManyToOne
	@JoinColumn(name="bossId", referencedColumnName = "idNumber")
	private Employee bossId;
	
	@OneToMany( mappedBy = "bossId")
	@JsonIgnore
	private List<Employee> employees;
	
}
