package org.review.employee.persistence;

import java.util.List;

import org.review.employee.entities.Employee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EmployeeRepository extends JpaRepository<Employee, String> {

	List<Employee> findByBossIdOrderByFullNameAsc(Employee boss);
	
}
