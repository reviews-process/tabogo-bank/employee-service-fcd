package org.review.employee.services;

import java.util.List;

import org.review.employee.entities.Employee;
import org.review.employee.exceptions.EmployeeException;

public interface EmployeeService {
	
	public void addEmployee(Employee newEmployee) throws EmployeeException;
	
	public List<Employee> getAll() throws EmployeeException;
	
	public List<Employee> findByBoss(String bossId) throws EmployeeException;
	
}
