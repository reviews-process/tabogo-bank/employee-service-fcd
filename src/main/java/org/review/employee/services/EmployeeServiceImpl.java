package org.review.employee.services;

import java.util.List;
import java.util.Optional;

import org.review.employee.entities.Employee;
import org.review.employee.exceptions.EmployeeException;
import org.review.employee.persistence.EmployeeRepository;
import org.review.employee.services.managers.ExceptionManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EmployeeServiceImpl extends ExceptionManager implements EmployeeService {

	private static final Logger LOGGER = LoggerFactory.getLogger(EmployeeServiceImpl.class);
	
	@Autowired
	private EmployeeRepository employeeRepository;
	
	private static final String ERRCODE_EMPTYRECORDS = "EMPTYRECS";
	
	private static final String ALREADY_EXISTS = "ALREADY";
	
	private static final String BOSS_NOT_EXISTS = "BOSSNA";
	
	@Override
	public void addEmployee(Employee newEmployee) throws EmployeeException {
		LOGGER.info("Creating Employee with id {} and name {}.", newEmployee.getIdNumber(), newEmployee.getFullName());
		try {
			if( validateEmployeeInDB(newEmployee.getIdNumber()) ) {
				LOGGER.error("Employee with id {} already was created!", newEmployee.getIdNumber());
				throw new EmployeeException(ALREADY_EXISTS, "The employee already was created!", new Exception());
			}
			if( newEmployee.getBossId() != null && !validateEmployeeInDB(newEmployee.getBossId().getIdNumber()) ) {
				LOGGER.error("The boss with id {} does not exist.", newEmployee.getBossId().getIdNumber());
				throw new EmployeeException(BOSS_NOT_EXISTS, "The boss does not exist!", new Exception());
			}
			this.employeeRepository.save(newEmployee);
			LOGGER.info("Employee identified with {} was successfully created!", newEmployee.getIdNumber());			
		}catch (Exception e) {
			throwCustomException(e);
		}
	}

	@Override
	public List<Employee> getAll() throws EmployeeException {
		List<Employee> employees = null;
		LOGGER.info("Getting all records of employees");
		try {
			employees = this.employeeRepository.findAll();
			if( employees.isEmpty() ) {
				throw new EmployeeException(ERRCODE_EMPTYRECORDS, "There is no employees registered!", new Exception());
			}
		}catch (Exception e) {
			throwCustomException(e);
		}
		return employees;
	}
	
	private boolean validateEmployeeInDB(String idNumber) {		
		Optional<Employee> employeeOpt =  this.employeeRepository.findById(idNumber);		
		return employeeOpt.isPresent();
	}

	@Override
	public List<Employee> findByBoss(String bossId) throws EmployeeException {
		List<Employee> employees = null;
		try {
			Employee filter = new Employee();
			filter.setIdNumber(bossId);
			employees = this.employeeRepository.findByBossIdOrderByFullNameAsc(filter);
			if( employees.isEmpty() ) {
				throw new EmployeeException(ERRCODE_EMPTYRECORDS, "There is no employees registered for the boss", new Exception());
			}
		}catch (Exception e) {
			throwCustomException(e);
		}
		return employees;
	}

}
