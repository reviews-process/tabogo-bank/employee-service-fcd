package org.review.employee.services.managers;

import javax.persistence.RollbackException;
import javax.validation.ConstraintViolationException;

import org.review.employee.exceptions.EmployeeException;

public class ExceptionManager {

	private static final String ERRCODE_EMPTYFIELDS = "EMPTYFIELD";
	private static final String ERRCODE_UNKNOWN = "UNKNOWN";
	
	protected void throwCustomException(Exception e) throws EmployeeException {
		if( e.getCause() instanceof RollbackException && ((RollbackException)e.getCause()).getCause() instanceof ConstraintViolationException) {
			ConstraintViolationException cve = (ConstraintViolationException)((RollbackException)e.getCause()).getCause();
			throw new EmployeeException(ERRCODE_EMPTYFIELDS, this.buildConstraintViolationMessage(cve), e);
		} else if( e instanceof EmployeeException ) {
			throw (EmployeeException)e;
		}else {
			throw new  EmployeeException(ERRCODE_UNKNOWN, e.getMessage(), e);
		}
	}
	
	private String buildConstraintViolationMessage( ConstraintViolationException e ) {
		StringBuilder sb = new StringBuilder("[");
		e.getConstraintViolations().stream().forEach(cv -> sb.append("'").append(cv.getMessage()).append("',"));
		sb.append("]");
		return sb.toString().replaceFirst(",]", "]");
	}
	
}
