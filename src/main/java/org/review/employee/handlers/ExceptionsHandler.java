package org.review.employee.handlers;

import org.review.employee.exceptions.EmployeeException;
import org.review.employee.model.EmployeeFault;
import org.review.employee.model.Issue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class ExceptionsHandler {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(ExceptionsHandler.class);
	
	@ExceptionHandler
	public ResponseEntity<Object> handleEmployeeException(EmployeeException e) {
		LOGGER.error("Application launches a custom exception", e);
		
		Issue issue = new Issue();
		issue.setCode(e.getCode());
		issue.setMessage(e.getMessage());
		
		return new ResponseEntity<>(new EmployeeFault(issue), HttpStatus.INTERNAL_SERVER_ERROR);
	}
	
	@ExceptionHandler
	public ResponseEntity<Object> handleException(Exception e) {
		LOGGER.error("Application launches an exception", e);
		
		Issue issue = new Issue();
		issue.setCode("GEN");
		issue.setMessage(e.getMessage());
		
		return new ResponseEntity<>(new EmployeeFault(issue), HttpStatus.INTERNAL_SERVER_ERROR);
	}
	
	@ExceptionHandler(MethodArgumentNotValidException.class)
	public ResponseEntity<Object> handleArgumentNotValidException(MethodArgumentNotValidException e) {
		
		Issue issue = new Issue();
		issue.setCode("AMNV");
		StringBuilder errMsg = new StringBuilder("");
		e.getBindingResult().getAllErrors().stream().forEach(err -> errMsg.append(err.getDefaultMessage()).append("\n"));
		issue.setMessage(errMsg.toString());
		
		return new ResponseEntity<>(new EmployeeFault(issue), HttpStatus.INTERNAL_SERVER_ERROR);
	}
	
}
