package org.review.employee.controllers;

import org.review.employee.exceptions.EmployeeException;
import org.review.employee.model.AddEmploye;
import org.review.employee.model.InquiryEmployees;
import org.review.employee.services.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin(methods = {RequestMethod.GET, RequestMethod.PUT})
public class EmployeeController {

	@Autowired
	private EmployeeService employeeService;
	
	@PutMapping("/employees")
	public void add(@RequestBody AddEmploye info) throws EmployeeException {
		this.employeeService.addEmployee(info.getEmployee());
	}
	
	@GetMapping("/employees")
	public ResponseEntity<InquiryEmployees> getAll() throws EmployeeException {
		InquiryEmployees response = new InquiryEmployees();
		response.setEmployees(this.employeeService.getAll());
		return new ResponseEntity<>(response, HttpStatus.OK);
	}
	
	@GetMapping("/employees/boss/{bossId}")
	public ResponseEntity<InquiryEmployees> findByBoss(@PathVariable String bossId) throws EmployeeException {
		InquiryEmployees response = new InquiryEmployees();
		response.setEmployees(this.employeeService.findByBoss(bossId));
		return new ResponseEntity<>(response, HttpStatus.OK);
	}
	
}
