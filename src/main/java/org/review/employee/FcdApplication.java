package org.review.employee;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FcdApplication {

	public static void main(String[] args) {
		SpringApplication.run(FcdApplication.class, args);
	}
	
}
