package org.review.employee.model;

import java.io.Serializable;
import java.util.List;

import org.review.employee.entities.Employee;

import lombok.Getter;
import lombok.Setter;

public class InquiryEmployees implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
		
	@Getter @Setter private List<Employee> employees;

}
