package org.review.employee.model;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

public class EmployeeFault implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Getter @Setter private Issue issue;
	
	public EmployeeFault(Issue issue) {
		this.issue = issue;
	}
	
}
