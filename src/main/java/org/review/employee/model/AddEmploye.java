package org.review.employee.model;

import java.io.Serializable;

import org.review.employee.entities.Employee;

import lombok.Getter;
import lombok.Setter;

public class AddEmploye implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Getter @Setter private Employee employee;

}
