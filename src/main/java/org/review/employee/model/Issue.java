package org.review.employee.model;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

public class Issue implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Getter @Setter private String code;
	
	@Getter	@Setter private String message;
	
}
