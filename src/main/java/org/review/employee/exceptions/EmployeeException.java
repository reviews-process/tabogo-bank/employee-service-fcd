package org.review.employee.exceptions;

import lombok.Getter;
import lombok.Setter;

public class EmployeeException extends Exception {

	@Getter @Setter private String code;
	
	@Getter @Setter private String message;
	
	public EmployeeException(String code, String message, Throwable cause) {
		super(cause);
		this.code = code;
		this.message = message;
	}
	
}
