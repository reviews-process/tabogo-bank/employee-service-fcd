INSERT INTO employees(id_number, full_name, area, role, boss_id) VALUES ('1130624259', 'Hugo Sanchez', 'IT', 'ITB', null);

-- Insert the BOSS
INSERT INTO employees(id_number, full_name, area, role, boss_id) VALUES ('2232334934', 'Cristiano Ronaldo', 'HR', 'HRO', null);
-- Insert his dependents
INSERT INTO employees(id_number, full_name, area, role, boss_id) VALUES ('1122334455', 'Frank Lampard', 'HR', 'HRA', '2232334934');
INSERT INTO employees(id_number, full_name, area, role, boss_id) VALUES ('5544332211', 'Lionel Messi', 'HR', 'HRA', '2232334934');
INSERT INTO employees(id_number, full_name, area, role, boss_id) VALUES ('6677889900', 'Edson Puch', 'HR', 'HRA', '2232334934');
INSERT INTO employees(id_number, full_name, area, role, boss_id) VALUES ('0099887766', 'Ruddy Voeller', 'HR', 'HRA', '2232334934');
INSERT INTO employees(id_number, full_name, area, role, boss_id) VALUES ('6655477337', 'Emilio Butragueño', 'HR', 'HRA', '2232334934');