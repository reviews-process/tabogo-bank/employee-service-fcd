package org.review.employee.services;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.review.employee.entities.Employee;
import org.review.employee.exceptions.EmployeeException;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
@TestMethodOrder(OrderAnnotation.class)
public class InquiryEmployeeTest {

	@Autowired
	private EmployeeService employeeService;
	
	@Test
	@Order(1)
	@DisplayName("Get all records In DB")
	void testFindAll() throws EmployeeException {
		List<Employee> employees = null;
		
		employees = this.employeeService.getAll();
		
		assertTrue(!employees.isEmpty(), "The query has no records.");
	}
	
	@Test
	@Order(2)
	@DisplayName("Get all records for a boss")
	void testFindByBoss() throws EmployeeException {
		List<Employee> employees = null;
		final String bossId = "2232334934";
		
		employees = this.employeeService.findByBoss(bossId);
		
		assertTrue(!employees.isEmpty(), "The query has no records.");
	}
	
	@Test
	@Order(3)
	@DisplayName("Boss without dependents")
	void testFindByBossEmpty() {
		EmployeeException employeeException = null;
		final String bossId = "2232334935";
		
		employeeException =  assertThrows(EmployeeException.class, ()->this.employeeService.findByBoss(bossId));
		
		assertEquals("EMPTYRECS", employeeException.getCode(), "The exception is not for empty records.");
	}
	
}
