package org.review.employee.services;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.review.employee.entities.Employee;
import org.review.employee.exceptions.EmployeeException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
@TestMethodOrder(OrderAnnotation.class)
public class AddEmployeeTest {

	@Autowired
	private EmployeeService employeeService;
	
	@Test
	@Order(1)
	@DisplayName("Adding an employee with null id")
	void testNullId() {
		EmployeeException exception = null;
		Employee employee = new Employee();
		employee.setFullName("Luis Revelo");
		employee.setRole("ITB");
		employee.setArea("IT");		
		
		exception = assertThrows(EmployeeException.class, ()->this.employeeService.addEmployee(employee));
		
		assertEquals("UNKNOWN", exception.getCode(), "Exception is not for empty field");
		assertTrue(exception.getMessage().contains(" id "), "Validation applied is not for Id Field");
	}
	
	@Test
	@Order(2)
	@DisplayName("Adding an employee with null name")
	void testNullName() {
		EmployeeException exception = null;
		Employee employee = new Employee();
		employee.setIdNumber("11223344");
		employee.setRole("ITB");
		employee.setArea("IT");		
		
		exception = assertThrows(EmployeeException.class, ()->this.employeeService.addEmployee(employee));
		
		assertEquals("EMPTYFIELD", exception.getCode(), "Exception is not for empty field");
		assertTrue(exception.getMessage().contains("Full Name"), "Validation applied is not for Full Name Field");	
	}
	
	@Test
	@Order(3)
	@DisplayName("Adding an employee with null role")
	void testNullRole() {
		EmployeeException exception = null;
		Employee employee = new Employee();
		employee.setIdNumber("11223344");
		employee.setFullName("Luis Revelo");
		employee.setArea("IT");		
		
		exception = assertThrows(EmployeeException.class, ()->this.employeeService.addEmployee(employee));
		
		assertEquals("EMPTYFIELD", exception.getCode(), "Exception is not for empty field");
		assertTrue(exception.getMessage().contains("Role "), "Validation applied is not for Role Field");			
	}
	
	@Test
	@Order(4)
	@DisplayName("Adding new employee with null area")
	void testNullArea() {
		EmployeeException exception = null;
		Employee employee = new Employee();
		employee.setIdNumber("11223344");
		employee.setFullName("Luis Revelo");
		employee.setRole("ITB");	
		
		exception = assertThrows(EmployeeException.class, ()->this.employeeService.addEmployee(employee));
		
		assertEquals("EMPTYFIELD", exception.getCode(), "Exception is not for empty field");
		assertTrue(exception.getMessage().contains("Area "), "Validation applied is not for Area Field");				
	}
	
	@Test
	@Order(5)
	@DisplayName("Trying to add an employee with existing id")
	void testExisting() {
		EmployeeException exception = null;
		Employee employee = new Employee();
		employee.setIdNumber("1130624259");
		employee.setFullName("Luis Revelo");
		employee.setRole("ITB");	
		employee.setArea("IT");	
		
		exception = assertThrows(EmployeeException.class, ()->this.employeeService.addEmployee(employee));
		
		assertEquals("ALREADY", exception.getCode(), "Exception is not for expected validation");
		assertTrue(exception.getMessage().contains("The employee already was created"), "Validation applied is not for the process");				
	}
	
	@Test
	@Order(6)
	@DisplayName("Adding new employee")
	void testAdd() {
		Employee employee = new Employee();
		employee.setIdNumber("11223344");
		employee.setFullName("Luis Revelo");
		employee.setRole("ITB");
		employee.setArea("IT");		
		
		assertDoesNotThrow(()-> this.employeeService.addEmployee(employee));		
	}
	
	@Test
	@Order(7)
	@DisplayName("Adding new employee with a false boss")
	void testAddWithNotExistentBoss() {
		EmployeeException exception = null;
		Employee employee = new Employee();
		employee.setIdNumber("11223355");
		employee.setFullName("Andriy Shevchenko");
		employee.setRole("ITB");
		employee.setArea("IT");	
		
		Employee boss = new Employee();
		boss.setIdNumber("443343");
		
		employee.setBossId(boss);
		
		exception = assertThrows(EmployeeException.class, ()->this.employeeService.addEmployee(employee));
		
		assertEquals("BOSSNA", exception.getCode(), "Exception is not for expected validation");
		assertTrue(exception.getMessage().contains("The boss does not exist"), "Validation applied is not for the process");		
	}
	
	@Test
	@Order(8)
	@DisplayName("Adding new employee with boss")
	void testAddWithBoss() {
		Employee employee = new Employee();
		employee.setIdNumber("11223355");
		employee.setFullName("Andriy Shevchenko");
		employee.setRole("ITB");
		employee.setArea("IT");	
		
		Employee boss = new Employee();
		boss.setIdNumber("11223344");
		
		employee.setBossId(boss);
		
		
		assertDoesNotThrow(()-> this.employeeService.addEmployee(employee));		
	}
	
	
	
}
